#include <iostream>;

class Animal
{
public:

	virtual void Voice()
	{
		std::cout << " ";
	}

};

class Dog : public Animal
{
	void Voice() override
	{
		std::cout << "Woof\n";
	}
};

class Cat : public Animal
{
	void Voice() override
	{
		std::cout << "Meow\n";
	}
};

class Mouse : public Animal
{
	void Voice() override
	{
		std::cout << "Squek\n";
	}
};

int main()
{
	
	Animal** array = new Animal* [3];
	array[0] = new Dog;
	array[1] = new Cat;
	array[2] = new Mouse;

	for (int i = 0; i < 3; i++)
	{
		array[i]->Voice();
	}

}